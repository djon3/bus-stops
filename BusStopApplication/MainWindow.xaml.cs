﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.XPath;

///Program: BusStopApplication.exe
///Author:  Jon Decher
///Purpose: A LTC bus stop reporting application that will allow the user to view the entire dataset, 
///         in a table, in Internet Explorer. In addition the program will allow the user to view formatted
///         information exclusively for stops that are on a selected street.
///Date:    August 9, 2013

namespace BusStopApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // File name constants
        private const string XML_FILE = @"..\..\..\ltcStops.xml";
        private const string XSLT_FILE_IN = @"..\..\..\ltcStops_in.xslt";
        private const string XSLT_FILE_OUT = @"..\..\..\ltcStops_out.xslt";

        public MainWindow()
        {
            InitializeComponent();
        }  
        
        /// <summary>
        /// Select all bus stops button. Will retrieve all information about the LTC bus routes
        /// and format it into an html table which can be viewed using Internet Explorer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Load the master stylesheet file into the DOM
                XmlDocument doc = new XmlDocument();
                doc.Load(XSLT_FILE_IN);

                // Obtain an XPathNavidator object
                XPathNavigator nav = doc.CreateNavigator();

                // Create a namespace manager object
                XmlNamespaceManager context = new XmlNamespaceManager(nav.NameTable);
                context.AddNamespace("xsl", "http://www.w3.org/1999/XSL/Transform");

                // Associate the namespace manager with the XPath expression
                XPathExpression expr = nav.Compile("//xsl:apply-templates/@select");
                expr.SetContext(context);

                // Execute the XPath expression
                XPathNodeIterator nodes = nav.Select(expr);

                if (nodes.MoveNext())
                {

                    // Build a new 'select' attribute string
                    string selectString = "//stop";

                    // Replace the select attribute
                    nodes.Current.SetValue(selectString);

                    // Write new XSLT doc
                    doc.Save(XSLT_FILE_OUT);

                    // Display the 'transformed' XML file in Internet Explorer

                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = "iexplore";
                    proc.StartInfo.Arguments = Directory.GetCurrentDirectory().ToString()
                        + @"\" + XML_FILE;
                    proc.Start();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }
        }

        /// <summary>
        /// Select particular bus stops button. Will retrieve all information about the LTC bus routes
        /// that have been inputed via the textbox on the GUI and format it into an html table which
        /// can be viewed using Internet Explorer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnParticular_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Load the master stylesheet file into the DOM
                XmlDocument doc = new XmlDocument();
                doc.Load(XSLT_FILE_IN);

                // Obtain an XPathNavidator object
                XPathNavigator nav = doc.CreateNavigator();              
                   
                // Create a namespace manager object
                XmlNamespaceManager context = new XmlNamespaceManager(nav.NameTable);
                context.AddNamespace("xsl", "http://www.w3.org/1999/XSL/Transform");

                //// Associate the namespace manager with the XPath expression
                XPathExpression expr = nav.Compile("//xsl:apply-templates/@select");
                expr.SetContext(context);
                XPathNodeIterator nodes = nav.Select(expr);

                XPathExpression varexpr = nav.Compile("//xsl:variable[@name='theBus']/@select");
                varexpr.SetContext(context);
                XPathNodeIterator varnode = nav.Select(varexpr);

                if (nodes.MoveNext())
                {
                    // Build a new 'select' attribute string
                    string selectString = "//stop[contains(@name, '" + streetText.Text + "')]";
                    // Replace the select attribute
                    nodes.Current.SetValue(selectString);

                    if(varnode.MoveNext())
                    {
                        //Set the title of the table
                        string busString = "'" + streetText.Text + "'" ;
                        varnode.Current.SetValue(busString);
                    }
                   
                    // Write new XSLT doc
                    doc.Save(XSLT_FILE_OUT);

                    // Display the 'transformed' XML file in Internet Explorer

                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = "iexplore";
                    proc.StartInfo.Arguments = Directory.GetCurrentDirectory().ToString()
                        + @"\" + XML_FILE;
                    proc.Start();
                }                               
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }
        }
    }
}
