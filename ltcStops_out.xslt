﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes" />
  <xsl:variable name="theBus" select="'Highbury'" />
  <xsl:variable name="theB" select="concat('LTC Stops for ', $theBus)" />
  <xsl:template match="/">
    <xsl:element name="html">
      <xsl:element name="body">
        <table style="width:720px; text-align:center" border="3">
          <tr>
            <col width="30%">
              <font face="Verdana" size="100">
                <xsl:value-of select="$theB" />
              </font>
            </col>
          </tr>
          <th>
            <font face="Verdana" size="4">Stop #</font>
          </th>
          <th>
            <font face="Verdana" size="4">Stop name</font>
          </th>
          <th>
            <font face="Verdana" size="4">Latitude</font>
          </th>
          <th>
            <font face="Verdana" size="4">Longitude</font>
          </th>
          <th>
            <font face="Verdana" size="4">Route</font>
          </th>
          <xsl:apply-templates select="//stop[contains(@name, 'Highbury')]">
            <xsl:sort select="@number" order="ascending" data-type="number" />
          </xsl:apply-templates>
        </table>
      </xsl:element>
    </xsl:element>
  </xsl:template>
  <xsl:template match="stop">
    <tr>
      <td>
        <xsl:value-of select="@number" />
      </td>
      <td>
        <xsl:value-of select="@name" />
      </td>
      <td>
        <xsl:value-of select="location/latitude" />
      </td>
      <td>
        <xsl:value-of select="location/longitude" />
      </td>
      <td>
        <xsl:value-of select="routes" />
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>